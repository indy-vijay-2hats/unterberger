import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue' 
import Wohnimmobilien from '../views/Wohnimmobilien.vue'
import Wohnimmobilien_detail from '../views/Wohnimmobilien_detail.vue'
import Aktuelle_projekte from '../views/Aktuelle_projekte.vue'


const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/wohnimmobilien',
    name: 'wohnimmobilien',
    component: Wohnimmobilien
  },
  {
    path: '/wohnimmobilien-detail',
    name: 'wohnimmobilien-detail',
    component: Wohnimmobilien_detail
  },
  {
    path: '/aktuelle-projekte',
    name: 'aktuelle-projekte',
    component: Aktuelle_projekte
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
