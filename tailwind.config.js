const colors = require('tailwindcss/colors')
module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: { 
    
    fontFamily: {
      'sans': ['"Univers LT 55"'],
      'serif':['"Superclarendon Lt"'],
      'mono':['"Univers LT 45"'],
      'cond':['"Univers LT 67 CondensedLt"'],  
      'nav':['"Univers LT 57 Condensed"'],
      'roman':['"UniversLTStd55Roman"'],
      '65cond':['"Univers65"']
     },
    
     container: {
        center: true, 
     },
    backgrounds:{
      white: '#FFFFFF',
      primary: '#262626',
      secondary: '#B3B3B3',  
      bggray:'#F2F2F2',
      graydrak:'#000000',
    }, 

    colors: { 
      transparent: 'transparent',
      current: 'currentColor',
      gray: colors.trueGray,
      red: colors.red,
      blue: colors.lightBlue,
      yellow: colors.amber, 
      white: '#FFFFFF',
      primary: '#262626',
      secondary: '#B3B3B3',  
      bggray:'#F2F2F2',
      graydrak:'#000000',
      txtgray :'#808080',
    },
    extend: { 
      letterSpacing: {
        headspace: '0.125em',
        smhead:'0.144em', 
        label: '0.044em',
        mainhd: '0.16em',
        fillabel:'0.05em',
        hrdspc:'0.15em',
        list:'0.02em',
        mainhdr:'0.386em',
        h4track:'0.089em'
      },
      maxWidth: {
       '3.5xl': '54rem',
       '2.5xl': '40.25rem',
       '10xl':'81.5',
      },
      maxHeight: {
        '97':'24.375rem',
        '98':'37.5rem',
        '625':'39.0625rem',
        '200':'12.5rem'
      },
      lineHeight: {
        'p':'1.6rem',
        '0':'0',
        '30':'1.875rem',
        '50':'3.125rem',
        '17':'1.0625rem',
        '10.5':'2.625rem',
        '28':'1.75rem',
        '26.5':'1.6rem'
      }, 
      padding: {
        '15': '3.75rem',
        '17': '4.3125rem',
        '18':'1.875rem',
        '30':'7.5rem',
        '23':'6.875rem',
        '22':'1.375rem',
        '21':'1.6875rem',
        'n3':'0.1875rem',
        'xm':'0.3125rem',
        '49':'3.6875rem',
        'hp':'0.3125rem',
        'fpb':'2.5625rem',
        'fpt':'2.3125rem',
        'lp':'0.8125rem',
        '126':'7.875rem',
        '113':'7.0625rem',
        '35':'2.1875rem',
        '121':'7.5625rem',
        '115':'0.9375rem',
        '551':'3.4375rem',
        '99':'0.5625rem',
        '43':'2.6875rem',
        '38':'2.375rem',
        '56':'3.5rem',
        '62':'3.875rem',
        '113':'7.0625rem',
        '124':'7.75rem'

      },      
      margin: {
        '-85': '-23.5rem',
        '-98': '-26rem', 
        '-57':'-15.5rem',
        '-xm':'0.3125rem',
        '26':'1.625rem',
        '-5nm':'-0.3125rem',
        '5sm':'0.3125rem',
        '22':'1.375rem',
       },
       spacing: {
        '18': '4.5rem', 
      },
      width:{
        '21': '5.25rem',
        '290':'18.125rem'
      },
      height: {
        '85':'26.875rem',
        '13':'0.8125rem',
        '49':'1.125rem',
        '424':'26.5rem'
      },
      fontSize: {
        'h1': '2.5rem',
        'h2': '2.25rem',
        'h3': '1.375rem',
        'h4': '1.125rem',
        'h5': '1rem',
        'h6': '0.875rem',
        '10':'0.625rem',
        '26':'1.625rem',
        '12':'0.75rem',
        '19':'1.1875rem',
        '20':'1.25rem'
      },
      
      inset: { 
       '82': '5.125rem', 
       'i1':'0.0625rem', 
       '-icon':'-0.875rem',
       'close':'0.1875rem'
      },
      boxShadow: {
        main :'0px 10px 99px rgba(0,0,0,0.30196078431372547)',
        form:'0px 10px 99px rgba(0,0,0,0.2)',
        nav:'0px 10px 20px rgba(0,0,0,0.10196078431372549)',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
